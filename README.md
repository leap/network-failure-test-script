# # Test script for network failures 

This is a simple script to test network failures. It (mostly) configures iptables rules to block network traffic to specific hosts/ports.

## Installation
You need [poetry](https://python-poetry.org/). Then run
> sudo poetry install

The script is built for Linux with iptables. To test the Android client, use the tool on a Linux Router. If you don't have a Linux router: You can also use a generic USB stick to spawn an Accesspoint (hostapd + dhcp server) and connect the mobile device with it.

## Features
```bash
pea@peabox:network-test-script sudo poetry run leap-con -h
usage: leap-con [-h] [-v] [-r] [-i INTERFACE] [--kill-openvpn-process] [--kill-tcp KILL_TCP] [--drop-host DROP_HOST] [--reject-host REJECT_HOST] [--drop-port DROP_PORT] [--reject-port REJECT_PORT]

options:
  -h, --help            show this help message and exit
  -v, --verbose         print verose output
  -r, --router          running in router mode. Modifying FORWARD and not OUTPUT chain
  -i INTERFACE, --interface INTERFACE
                        interface used for blocking commands
  --kill-openvpn-process
                        Kill openvpn process
  --kill-tcp KILL_TCP   Kills all established tcp connections. Needs: port as argument
  --drop-host DROP_HOST
                        Drops traffic to a certain ip or hostname. Needs ip/hostname
  --reject-host REJECT_HOST
                        Reject traffic to a certain ip or hostname. Needs ip/hostname
  --drop-port DROP_PORT
                        Drops traffic to a certain port
  --reject-port REJECT_PORT
                        Reject traffic to a certain port
```

## Connections that can be blocked

- message of the day
- update check
- API backend (gateways, locations, openvpn arguments)
- connections to OpenVPN gateways
- local management connection

## Not supported by the tool, but helpful

### Redirect traffic

Use the following iptables rules:
```
iptables -t nat -A OUTPUT -p tcp -d 198.252.153.107 --dport 4430 -j DNAT --to-destination 193.99.144.80:443
iptables -t nat -A OUTPUT -p tcp -d 198.252.153.107 --dport 443 -j DNAT --to-destination 193.99.144.80:443
```
These commands redirect traffic to 198.252.153.107 (port 443/4430) to 193.99.144.80:443. We can test invalid ssl certificates with it. If you are on a Router, use `PREROUTING` instead of `OUTPUT` chain.

### Spoof DNS
Just add hosts to `/etc/hosts`
