import sys
import os
import logging
import subprocess
import socket
import argparse
import shutil
import time

DEPENDENCIES = ["tcpkill", "timeout"]

FORMAT = "%(levelname)s: %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)


def fail(msg: str):
    logging.error(msg)
    sys.exit(1)


def check_deps():
    logging.debug("Checking script dependencies")
    for file in DEPENDENCIES:
        if not shutil.which(file):
            logging.warning(f"Could not find dependency in PATH: {file}")


#def load_config(provider: str) -> dict[str: Any]:
#    config_file = Path(provider + ".yml")
#    if not config_file.exists():
#        fail(f"Could not open config file {config_file.absolute()}")
#
#    with config_file.open() as f:
#        conf = yaml.safe_load(f)
#    return conf


def run(cmd: str):
    logging.info(f"Executing: {cmd}")
    try:
        p = subprocess.run(cmd, shell=True, capture_output=True, check=True)
        if p.stdout.decode().strip() != "":
            logging.debug(f"Response: {p.stdout.decode()}")
    except subprocess.CalledProcessError as e:
        fail(f"Command '{cmd}' returned with exit code {e.returncode}. " +
             f"Output:\n{e.stderr.decode().strip()}")


def kill_process(process_name: str):
    cmd = f"killall -9 {process_name}"
    run(cmd)
    logging.info(f"Sucessfully stopped {process_name}")


def break_host_connection(host: str, method: str, router_mode: bool):
    chain = "FORWARD" if router_mode else "OUTPUT"
    logging.debug(f"Breaking conection to host {host} (method {method}")
    try:
        _, _, ip_addresses = socket.gethostbyname_ex(host)
    except socket.gaierror as e:
        fail(f"Could not resolve host '{host}': {e}")

    for ip in ip_addresses:
        logging.info(f"Dropping traffic to {ip} {method}")
        cmd = f"iptables -I {chain} 1 -d {ip} -j {method.upper()}"
        run(cmd)

    logging.info("Please enter ctrl-c to remove iptable rules")
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for ip in ip_addresses:
            logging.info(f"Re-enabling traffic to {ip}")
            cmd = f"iptables -D {chain} 1"
            run(cmd)


def break_port_connection(port: int, method: str, router_mode: bool):
    logging.info(f"Breaking conection (udp and tcp) to port {port} (method {method}")
    chain = "FORWARD" if router_mode else "OUTPUT"
    run(f"iptables -I {chain} 1 -p tcp --dport {port} -j {method.upper()}")
    run(f"iptables -I {chain} 1 -p udp --dport {port} -j {method.upper()}")

    logging.info("Please enter ctrl-c to remove iptables rules")
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        logging.info(f"Re-enabling traffic on port {port}")
        run(f"iptables -D {chain} 1")
        run(f"iptables -D {chain} 1")


def kill_tcp_connection(interface: str, port: str):
    logging.debug(
        f"Closing all tcp connections on port {port} for the next 10 seconds")
    cmd = f"timeout --preserve-status 10 tcpkill -i {interface} port {port}"
    try:
        run(cmd)
        logging.info(f"Sucessfully killed all running tcp connections on port {port}")
    except KeyboardInterrupt:
        pass


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        help="print verose output")
    parser.add_argument("-r", "--router",
                        action="store_true",
                        help="running in router mode. Modifying FORWARD and not OUTPUT chain")
    parser.add_argument("-i", "--interface",
                        help="interface used for blocking commands")
    parser.add_argument("--kill-openvpn-process",
                        action="store_true",
                        help="Kill openvpn process")
    parser.add_argument("--kill-tcp",
                        help="Kills all established tcp connections. Needs: port as argument")
    parser.add_argument("--drop-host",
                        help="Drops traffic to a certain ip or hostname. Needs ip/hostname")
    parser.add_argument("--reject-host",
                        help="Reject traffic to a certain ip or hostname. Needs ip/hostname")
    parser.add_argument("--drop-port",
                        help="Drops traffic to a certain port")
    parser.add_argument("--reject-port",
                        help="Reject traffic to a certain port")

    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(0)
    else:
        return parser.parse_args()


def main():
    if os.getuid() != 0:
        fail("This needs to be run as root")

    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    check_deps()

    if args.kill_openvpn_process:
        process_name = "openvpn"
        kill_process(process_name)
    elif args.kill_tcp:
        if not args.interface:
            fail("-i/--interface must be specified")
        kill_tcp_connection(args.interface, args.kill_tcp)
    elif args.drop_host:
        break_host_connection(args.drop_host, "drop", args.router)
    elif args.reject_host:
        break_host_connection(args.reject_host, "reject", args.router)
    elif args.drop_port:
        break_port_connection(args.drop_port, "drop", args.router)
    elif args.reject_port:
        break_port_connection(args.reject_port, "reject", args.router)


if __name__ == '__main__':
    main()
